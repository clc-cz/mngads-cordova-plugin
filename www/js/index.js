/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');

    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
        this.initMngAds();
        
    },
    mngadsAppId: function() {
      return ( /(android)/i.test(navigator.userAgent) ) ?'YOUR_ANDROID_APPID':'YOUR_IOS_APPID';
    },
    initMngAds: function() {
        // Set your AppId.
        this.mngAds = new MngAdsSDK();
        if (! this.mngAds ) { alert( 'mngAds plugin not ready' ); return; }
        // Connection to the API.
	this.mngAds.setDebugMode(true);
        this.mngAds.initWithAppId(this.mngadsAppId(),this.onInitialize);
        this.mngAds.isInitialized(this.isInitialized);
        document.getElementById("status-ads").innerHTML="mngAds is ready ! ";
        
    },
    createInterstitial: function() {
	var age = "25";
	var language = "fr";
	var keyword="brand=myBrand;category=sport;";
	var gender="M";
	var location={"lat":48.876,"lon":10.453};
	var preferences = {
   	 "age": age,
   	 "language": language, 
    	 "keyword":keyword,
    	 "gender":gender,
    	 "location":location     	
	}
      
    // Set your placementId.
    var placementId = '/'+this.mngadsAppId()+'/interstitial';
    document.getElementById("status-ads").innerHTML="waiting Interstitial "+placementId ;
    this.mngAds.createInterstitial(placementId,preferences,this.adSuccess,this.adError);
      
    },
    createBanner: function(id,size,position) {
	var age = "25";
	var language = "fr";
	var keyword="brand=myBrand;category=sport;";
	var gender="M";
	var location={"lat":48.876,"lon":10.453};
	var preferences = {
    	 "age": age,
    	 "language": language, 
   	 "keyword":keyword,
    	 "gender":gender,
   	 "location":location
        }
      // Set your placementId.
      var placementId = '/'+this.mngadsAppId()+'/homebanner';
      document.getElementById("status-ads").innerHTML="waiting Interstitial "+placementId ;
      this.mngAds.createBanner(placementId,size,position,true,preferences,this.adSuccess,this.adError)
    },
    showBanner: function() {
      this.mngAds.showBanner(this.adSuccess,this.adError);
    }, 
    isInitialized: function(ok) {
        if (ok == true) {
          console.log("MNGAds is initialized");     
      }else{
           console.log("MNGAds is not initialized");   
      }
    }, 
     onInitialize: function() {
	
     console.log("MNGAds is initialized"); 

       app.createInterstitial();
    }, 	
    adError: function(error) {
       document.getElementById("status-ads").innerHTML=error;
    },
    adSuccess: function(eventType) {
      if (eventType == "DID_LOAD") {
          console.log("didLoad");
          document.getElementById("status-ads").innerHTML="success - didLoad";
      }else{
           console.log("didDisappear");
           document.getElementById("status-ads").innerHTML="success - didDisappear";
      }
  }
};

function clickAutoDisplay(){
    if (document.getElementById("autoDispaly").checked) {
        document.getElementById("btnShow").setAttribute('style', 'display:none;');
    }else{
        document.getElementById("btnShow").setAttribute('style', 'display:inline;');
    };
}


